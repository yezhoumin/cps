# C语言程序设计题库

## 实验项目一

1. 编写一个程序，要求用户输入一个美元数量，然后显示出增加5%税率后的相应金额，格式如下：

        Enter an amount: 100.00
        With tax added: $105.00


2. 编写一个程序，要求用户输出一个美元数量，然后显示出如何用最少20美元、10美元、5美元和1美元来付款：

        Enter a dollar amount: 93
        $20 bills: 4
        $10 bills: 1
        $5  bills: 0
        $1  bills: 3

3. 编程计算第一、第二、第三个月还贷后剩余的贷款金额：

        Enter amount of loan: 20000.00
        Enter interest rate: 6.0
        Enter monthly payment: 386.66

        Balance remaining after first payment: $19713.34
        Balance remaining after second payment: $19425.25
        Balance remaining after third payment: $19135.71

4. 编写一个程序，以月/日/年的格式接受用户录入的日期信息，并以年月日的格式将其显示出来：

        Enter a date (mm/dd/yyyy): 2/17/2011
        You entered the date: 20110217

5. 修改3.2节的`addfrac.c`程序，使用户可以同时输入两个分数，中间用加号隔开：

        Enter two fractions separated by a plus sign: 5/6+3/4
        The sum is 38/24

    
## 实验项目二
1. 编写一个程序，要求用户输入 24 小时制的时间，然后显示 12 小时制的格式：

        Enter a 24-hour time: 21:11
        Equivalent 12-hour time: 9:11 PM

2. 编写一个程序，要求用户输入风速（海里/小时），然后显示相应的描述。下面是用于测量风力的蒲福风力等级的简化版。

    速率（海里/小时）| 描述
    ----|----
    <1     | Calm（无风）
    1~3    | Light air（轻风）
    4~27   | Breeze（微风）
    28~47  | Gale（大风）
    48~63  | Storm（暴风）
    大于 63| Hurricane（飓风）

3. 修改教材4.1节的 `upc.c` 程序，使其可以检测UPC的有效性。在用户输入 UPC 后，程序将显示 `VALID` 或 `NOT VALID`。

4. 利用 switch 语句编写一个程序，把用数字表示的成绩转化为字母表示的等级。
    Tips：使用下面的等级评定规则：
    
    A 为 90~100，B 为 80~89，C 为 70~79，D为 60~69，F 为 0~59。如果成绩高于100或低于0显示出错信息。把成绩拆分成2个数字，然后使用`switch`语句判定十位上的数字。

        Enter numerical grade：84
        Letter grade：B

5. 编写程序，要求用户输入两个整数，然后计算这两个整数的最大公约数（GCD）

        Enter two integers：12 28
        Greatest common divisor：4

6. 在5.2节的`broker.c`程序中添加循环，以便用户可以输入多笔交易并且程序可以计算每次的佣金。程序在用户输入的交易额为`0`时终止。

        Enter value of trade：30000
        Commission：$166.00
        Enter value of trade：20000
        Commission：$144.00
        Enter value of trade：0

7. 编写程序，提示用户输入一个数 n，然后显示 出 1~n 的所有偶数平方值。例如，如果用户输入 100，那么程序应该显示出下列内容：

        4
        16
        36
        64
        100

8. 编写程序显示单月的日历。用户指定这个月的天数和该月起始日是星期几。

        Enter number of days in month：31
        Enter starting day of the week(1=Sun, 7=Sat): 3
               1  2  3  4  5
        6  7  8  9  10 11 12
        13 14 15 16 17 18 19
        20 21 22 23 24 25 26
        27 28 29 30 31

## 实验项目三

1. 编写程序可以把字母格式的电话号码翻译成数值格式:

        Enter phone number: CALLATT
        2255288

    如果没有电话在身边,参考这里给出的字母在键盘上的对应关系：
    
    按键|字母
    ----|----
    2|ABC
    3|DEF
    4|GHI
    5|JKL
    6|MNO
    7|PQRS
    8|TUV
    9|WXYZ
    
    原始电话号码中的非字母字符（例如数字或标点符号）保持不变:

        Enter phone number :1-800-COL-LECT
        1-800-265-5328

    可以假设任何用户输入的字母都是大写字母。

2. 编写程序对表达式求值:

        Enter an expression: 1+2.5*3
        Value of expression: 10.5

    表达式中的操作数是浮点数,运算符是+、-、*和/。表达式从左向右求值（所有运算符的优先级都一样）。

3. 输入数字，打印一份列表，显示出每个数字在其中出现的次数。

        Enter a number: 41271092
        Digit:        0  1  2  3  4  5  6  7  8  9
        Occurrences:  1  2  2  0  1  0  0  1  0  1

4. 编写程序,生成一种贯穿10×10字符数组（初始时全为字符’.）的“随机步法”。程序必须随机地从一个元素“走到”另一个元素,每次都向上、向下、向左或向右移动一个元素位置。已访问过的元素按访问顺序用字母A到Z进行标记。下面是一个输出示例:

        A . . . . . . . . .
        B C D . . . . . . .
        . F E . . . . . . .
        H G . . . . . . . .
        I . . . . . . . . .
        J . . . . . . . Z .
        K . . R S T U V Y .
        L M P Q . . . W X .
        . N O . . . . . . .
        . . . . . . . . . .

    提示:利用 `srand`函数和`rand`函数(见程序`dea1.c`)产生随机数,然后查看此数除以4的余数。余数一共有4种可能的值(0、1、2和3),指示下一次移动的4种可能方向。在执行移动之前,需要检查两项内容:一是不能走到数组外面,二是不能走到已有字母标记的位置。只要有一个条件不满足,就得尝试换一个方向移动。如果4个方向都堵住了,程序就必须终止了。下面是提前结束的一个示例:
    
        A B G H I . . . . .
        . C F . J K . . . .
        . D E . M L . . . .
        . . W X Y P Q . . .
        . . V U T S R . . .
        . . . . . . . . . .
        . . . . . . . . . .
        . . . . . . . . . .
        . . . . . . . . . .

    因为Y的4个方向都堵住了,所以没有地方可以放置下一步的Z了。

5. 已知的最古老的一种加密技术是凯撒加密(得名于 Julius caesar)。该方法把一条消息中的每个字母用字母表中固定距离之后的那个字母来替代。(如果越过了字母Z,会绕回到字母表的起始位置。例如,如果每个字母都用字母表中两个位置之后的字母代替,那么Y就被替换为A,Z就被替换为B。)编写程序用凯撒加密方法对消息进行加密。用户输入待加密的消息和移位计数(字母移动的位置数目):

        Enter message to be encrypted: Go ahead, make my day.
        Enter shift amount (1-25):3
        Encrypted message: Jr dkhdg, pdh pb gdb.

    注意,当用户输入26与移位计数的差值时,程序可以对消息进行解密:

        Enter message to be encrypted: Jr dkhdg, pdh pb gdb.
        Enter shift amount (1-25):23
        Encrypted message: Go ahead, make my day.

    可以假定消息的长度不超过80个字符。不是字母的那些字符不要改动。此外,加密时不要改变字母的大小写。提示:为了解决前面提到的绕回问题,可以用表达式`((ch-'A')+n)%26 + 'A'`计算大写字母的密码,其中ch存储字母,n存储移位计数。(小写字母也需要一个类似的表达式。)

6. 修改题目4，使其包含下列函数：

        void generate_random_walk(char walk[10][10]);
        void print_array(char walk[10][10]);

    main函数首先调用`generate_random_walk`，该函数把所有数组元素都初始化为字符‘ .’，然后将其中一些字符替换为A到Z的字母详见题目4，接着main函数调用`print_array`函数来显示数组。

## 实验项目四
1. 修改10.2节的栈示例使它存储字符而不是整数。接下来,增加main函数,用来要求用户输入一串圆括号或花括号,然后指出它们之间的嵌套是否正确。

        Enter parenteses and/or braces: ((){}{()}}
        Parenteses/braces are nested properly

    提示:读入左圆括号或左花括号时,把它们像字符一样压入栈中。当读入右圆括号或右花括号时,把栈顶的项弹出,并且检查弹出项是否是匹配的圆括号或花括号。(如果不是,那么圆括号或花括号嵌套不正确。)当程序读入换行符时,检查栈是否为空。如果为空,那么圆括号或花括号匹配;如果栈不为空(或者如果曾经调用过 `stack_underflow`函数),那么圆括号或花括号不匹配。如果调用`stack_underflow`函数,程序显示信息`Stack overflow`,并且立刻终止。未调用此函数，若匹配，则打印`Parenteses/braces are nested properly`，否则打印`Parenteses/braces are not nested properly`。


2. 修改10.5节的 `poker.c`程序,把所有的外部变量移到main函数中,并修改各个函数使它们通过参数进行通信。 `analyze_hand`函数需要修改变量 `straight`、 `flush`、`four`、 `three`和 `pairs`,所以它需要以指向这些变量的指针作为参数。（`pocker.c`程序位于c语言程序设计现代方法2的`p.166`）

3. 编写程序读一条消息,然后逆序打印出这条消息，输入字符串最大长度为1024个字符:


        Enter a message: Don't get mad, get even.
        Reversal is: .neve teg ,dam teg t'noD

    提示:一次读取消息中的一个字符(用 `getchar`函数),并且把这些字符存储在数组中,当数组满了或者读到字符'\n'时停止读操作。

4. 修改题目3程序，用指针代替整数来跟踪数组中的当前位置。

5. 请利用数组名可以用作指针的事实简化题目4中的程序。

6. 编写程序找出一组单词中“最小”单词和“最大”单词。用户输入单词后,程序根据字典顺序决定排在最前面和最后面的单词。当用户输入4个字母的单词时,程序停止读入。假设所有单词都不超过20个字母。程序会话如下:

        Enter word: dog
        Enter word: zebra
        Enter word: rabbit
        Enter word: catfish
        Enter word: walrus
        Enter word: cat
        Enter word: fish


        Smallest word: cat
        Largest word zebra

    提示:使用两个名为`smallest_word`和`largest_word`的字符串来分别记录所有输入中的“最小”单词和“最大”单词。用户每输入一个新单词,都要用 `strcmp`函数把它与`smallest_word`进行比较如果新的单词比`smallest_word`“小”,就用 `strcpy`函数把新单词保存到`smallest_word`中。用类似的方式与`largest_word`进行比较。用`strlen`函数来判断用户是否输入了4个字母的单词。
