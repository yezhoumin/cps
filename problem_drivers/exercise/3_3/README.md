# 3_3 分解ISBN信息 Resolve ISBN information

## 题目

图书用国际标准书号（ISBN）进行识别。2007年1月1日之后分配的 ISBN 包含 13位数字（旧的 ISBN 使用 10 位数字），分为5组，如 978-0-393-97950-3。第一组（GS1前缀）目前为978或979。第二组（组标识） 指明语言或者原出版国（如 0 和 1 用于讲英语的国家）。第三组（出版商编号）表示出版商（ 393 是 W.W.Norton出版社的编号）。第四组（产品编号）是由出版商分配的用于标识具体哪一本书的（97950）。ISBN 的末尾是一个校验数字，用于验证前面的数字的准确性。编写一个程序来分解用户录入的 ISBN 信息。

## 样例

### 样例一

        Enter ISBN: 978-0-393-97950-3
        GS1 prefix: 978
        Group identifier: 0
        Publisher code: 393
        Iten number: 97950
        Check digit: 3

### 样例二

        Enter ISBN: 978-1-392-95540-4
        GS1 prefix: 978
        Group identifier: 1
        Publisher code: 392
        Iten number: 95540
        Check digit: 4
        
## 数据范围

输入的输均为整数。
按照题目内容中的输出为规范输出。

## 提示

每组数字的个数是可变的，不能认为每组的长度都与示例一样。用实际的 ISBN 值（通常放在书的封底和版权页上）测试你编写的程序。