### 11-4：

修改 10.5 节的 pocker.c 程序，把所有的外部变量移到 main 函数中，并修改各个函数使它们通过参数进行通信。analyze_hand函数需要修改变量 straight、flush、four、three 和 pairs，所以它需要以指向这些变量的指针作为参数。