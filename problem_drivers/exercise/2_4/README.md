# 2_4 输出税后金额 Output tax amount

## 题目

编写一个程序，要求用户输入一个美元数量，然后显示出增加 5% 税率后的相应金额。

## 样例

### 样例一

    Enter an amount: 100.00
    with tax added: $105.00

### 样例二

    Enter an amount: 200.00
    with tax added: $210.00
    
## 数据范围

输入的金额为大于0的浮点数。       
按照题目内容中的输出为规范格式进行输出。