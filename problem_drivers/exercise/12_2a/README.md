﻿### 第 12 章编程题 2(a)
编写程序读一条消息，然后检查这条消息是否回文（消息中的字母从左往右看和从右往左看是
一样的）：
     
     Enter a message: He lived as devil, eh?
     Palindrome.
     
     Enter a message: Madam, I am Adam.
     Not a palindrome.
忽略所有不是字母的字符。用整形变量来跟踪数组中的位置。




