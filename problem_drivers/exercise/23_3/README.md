# 23_3 首字母大写 Uppercase

## 题目

编写一个程序，将文本文件从标准输入复制到标准输入，将每个单词的首字母大写。

## 样例

### 样例一

      my friend
      My Friend

### 样例二

      welcome to C world
      Welcome To C World

## 数据范围

输入的字符从键盘获取，为英文单词。  
按照题目内容中的输出为规范输出。




