### 7-2：

 修改 6.3 节的程序 square.c，每 24 次平方后暂停并显示下列信息：

		Press Enter to continue...
显示完上述消息后，程序应该使用 getchar 函数读入一个字符。getchar 函数读到用户录入的回车键才允许程序继续。
