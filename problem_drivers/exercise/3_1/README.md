# 3_1 日期转换 Date conversion

## 题目

编写一个程序，以月/日/年（即 mm/dd/yy）的格式接受用户录入的日期信息，并以年月日（即 yyyymmdd）的格式将其显示出来。

## 样例

### 样例一

        Enter a date (mm/dd/yyyy) : 2/17/2011
        You entered the date 20110217

### 样例二

        Enter a date (mm/dd/yyyy) : 3/11/2018
        You entered the date 20180311

## 数据范围

所输入的日期必须符合正常范畴。
按照题目内容中的输出为规范输出。


