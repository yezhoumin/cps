## 第六章

1. 编写程序，找出用户输入的一串数中的最大数。程序需要提示用户一个一个地输入数。当用户输入 0 或负数时，程序必须显示出已输入的最大非负数：

        Enter a number: 60
        Enter a number: 38.3
        Enter a number: 4.89
        Enter a number: 100.62
        Enter a number: 75.2295
        Enter a number: 0
        The largest number entered was 100.62

    注意输入的数不一定是整数。