# 23_4 相反顺序显示 Reverse sequential display

## 题目
 
编写一个程序，提示用户输入一系列单词，单词之间用一个空格隔开，然后按照相反的顺序显示出来。

## 样例

### 样例一

	Please Enter the String: crfomko moewcm fmeoim fmcok emkfoce
	emkfoce fmcok fmeoim moewcm crfomko

### 样例二

	Please Enter the String: welcome to C world
	world C to welcome

## 数据范围

输入单词。
按照题目内容中的输出为规范输出。

## 提示

将输入按照字符串的形式读入，然后使用 strtok 函数将它们分隔成单词。