### 11-3：

修改第 6 章的编程题 3，使其包含下列函数：

			void reduce(int numerator，int denominator，int *reduced_numerator，int *reduced_denominator)：

numerator 和 denominator 分别是分数的分子和分母。 reduced_numerator 和 reduced_denominator 是指向变量的指针，相应变量中分别存储把分数化为最简形式后的分子和分母。
