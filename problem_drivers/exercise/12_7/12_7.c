#include <stdio.h>

#define  N  10

void max_min(int a[], int n, int *max, int *min);

int main(int argc, char *argv)
{
	int b[N], i, big, small;

	printf("Enter %d numbers: ", N);
	for (i = 0; i < N; i++)
		scanf("%d", &b[i]);

	max_min(b, N, &big, &small);

	printf("Largest: %d\n", big);
	printf("Smallest: %d\n", small);

	return 0;
}

void max_min(int a[], int n, int *max, int *min)
{
	int *b = &a[1];
	int *c = &a[n - 1];

	*max = *min = a[0];

	for (; b <= c; b++) {
		if (*b > *max)
			*max = *b;
		else if (*b < *min)
			*min = *b;
	}
}