#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#define BUFFERSIZE 100

int main(int argc, char *argv)
{
	char buffer[BUFFERSIZE];
	memset(buffer, 0, BUFFERSIZE);
	int length;

    printf("Enter a message: ");
    fgets(buffer, sizeof(buffer), stdin);
    length = strlen(buffer);
    char *a = buffer;
    char *b = &buffer[length - 2];

    for (int i = 0; i < length - 1; i++)
    {
    	if (isupper(buffer[i]))
			buffer[i] = tolower(buffer[i]);
    }

    for (; a <= b ;)
    {
    	if (*a < 97 || *b < 97) {
    		if (*a < 97)
    			a++;
    		if (*b < 97)
    			b--;
    		continue;
    	}
    	if (*a != *b) {
    		printf("Not a Palindrome\n");
    		exit(1);
    	}
    	a++;
    	b--;
    }

    printf("Palindrome\n");
}