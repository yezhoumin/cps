# 23_2 标准输入复制到标准输出 Input copy to output

## 题目

编写一个程序，将文本文件从标准输入复制到标准输出，并删除每行开头的空白字符。不要复制仅包含空白字符的行。

## 样例

### 样例一

           isjcnucjdnicjn
    jicocjsikcjiosdc

### 样例二

           fafawefvzxdv
    fafawefvzxdv

## 数据范围

通过键盘将文本输入。              
按照题目内容中的输出为规范输出。

