# 3_5 计算和 Calculate sums

## 题目

编写一个程序，要求用户（按任意次序）输入从1到16的所有整数。然后用4x4矩阵的形式将它们显示出来，再计算出每行，每列和每条对角线上的和。

## 样例

### 样例一

        Enter the numbers from 1 to 16 in any order: 
        16 3 2 13 5 10 11 8 9 6 7 12 4 15 14 1        
                                   
        16  3  2 13
         5 10 11  8
         9  6  7 12
         4 15 14  1

        Rows sums: 34 34 34 34   
        Column sums: 34 34 34 34  
        Diagonal sums: 34 34      
  
### 样例二

        Enter the numbers from 1 to 16 in any order: 
        1 3 5 7 12 15 16 2 4 6 8 9 10 11 13 14       
                                   
         1  3  5  7
        12 15 16  2
         4  6  8  9
        10 11 13 14

        Rows sums: 16 45 27 48
        Column sums: 27 35 42 32  
        Diagonal sums: 38 39   

## 数据范围

输入的数为1-16的整数。          
按照题目内容中的输出为规范输出。

