### 7-1：

  如果 i * i 超出了 int 类型的最大取值，那么 6.3 节的程序 square2.c 将失败（通常会显示奇怪的答案）。运行该程序，并确定导致失败的 n 的最小值。尝试把变量 i 的类型改成 short 并再次运行该程序。（不要忘记更新 printf 函数调用中的转换说明！）然后常识改成 long 从这些实验中，你能总结出在你的机器上用于存储整数类型的位数是多少吗？
	
		请把你的答案替换程序中的 a、b、c、e、f 中并且提交
		int main(void)
		{
		printf("%d,%d,%ld,%d,%d,%d",a,b,c,d,e,f);
		return 0;
		}
注： a 为原程序中 int 类型下导致失败的 n 的最小值，b 为更改为 short 类型后，导致失败的 n 的最小值，c 为更改为 long 类型后，导致失败的 n 的最小值。 d 为 int 类型存储整型位数，e 为 short 类型存储整型位数，f 为 long 类型存储整型位数。
提示：导致失败的最小值与位数的关系为 2^(n-1) - 1。