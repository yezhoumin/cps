#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define BUFFERSIZE 80

int main(int argc, char *argv[])
{
	char buffer[BUFFERSIZE];
	memset(buffer, 0, BUFFERSIZE);
	int length = 0;
    char signal = 0;
    int temp1 = 0;
    int temp2 = 0;
    int index = 0;

	printf("Enter a sentence: ");
	fgets(buffer, sizeof(buffer), stdin);
	length = strlen(buffer);
	char str[length + 1];
    signal = buffer[length - 2];
    temp2 = length - 3;

    for(int i = length - 3; i > 0; i--) {
    	if (buffer[i] < 33) {
    		temp1 = i + 1;
    		for (int j = 0; j <= temp2 - temp1; j++)
    			str[index + j] = buffer[temp1 + j];
    		index = index + temp2 - temp1;
    		str[++index] = ' ';
    		index++;
    		temp2 = i - 1;
    	}
    }

    temp1--;
    for(int i = 0; i < temp1; i++)
    	str[index + i] = buffer[i];

    str[length - 2] = signal;
    str[length - 1] = '\n';
    str[length] = '\0';

    printf("Reversal of sentence: %s", str);

}