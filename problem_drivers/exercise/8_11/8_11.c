#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define BUFFERSIZE 17
#define SIZE    15

void replace(char *sendbuf, int length);

int main(int argc, char *argv[])
{
	char buffer[BUFFERSIZE];
	memset(buffer, 0, BUFFERSIZE);
	int length;
    
    printf("Enter phone number: ");
	fgets(buffer, sizeof(buffer), stdin);
	length = strlen(buffer);
	replace(buffer, length);
	buffer[length] = '\0';
	printf("In numeric form: %s", buffer);

}

void replace(char *sendbuf, int length)
{
	char temp;
	for (int i = 0; i < length; i++) {
		temp = sendbuf[i];
		if ((temp >= 'A') && (temp <= 'Z')) {
			if (temp == 'A' || temp == 'B' || temp == 'C')
				sendbuf[i] = '2';
			else if (temp == 'D' || temp == 'E' || temp == 'F')
				sendbuf[i] = '3';
			else if (temp == 'G' || temp == 'H' || temp == 'I')
				sendbuf[i] = '4';
			else if (temp == 'J' || temp == 'K' || temp == 'L')
				sendbuf[i] = '5';
			else if (temp == 'M' || temp == 'N' || temp == 'O')
				sendbuf[i] = '6';
			else if (temp == 'P' || temp == 'Q' || temp == 'R' || temp == 'S')
				sendbuf[i] = '7';
			else if (temp == 'T' || temp == 'U' || temp == 'V')
				sendbuf[i] = '8';
			else if (temp == 'W' || temp == 'X' || temp == 'Y' || temp == 'Z')
				sendbuf[i] = '9';
		}
	}
}