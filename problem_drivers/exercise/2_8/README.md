# 2_8 贷款金额 Loan amount

## 题目

编程计算第一、第二、第三个月还贷后剩余的贷款金额。

## 样例

### 样例一

        Enter amout of loan: 20000.00
        Enter interest rate: 6.0
        Enter monthly payment: 386.66

        Balance remaining after first payment: $19713.34
        Balance remaining after second payment: $19425.25
        Balance remaining after third payment: $19135.71

### 样例二

        Enter amout of loan: 20000.00
        Enter interest rate: 5.0
        Enter monthly payment: 396.66

        Balance remaining after first payment: $19686.67
        Balance remaining after second payment: $19372.04
        Balance remaining after third payment: $19056.10

## 数据范围

输入的贷款金额值为大于 0 的浮点数。    
按照题目内容中的输出为规范输出。

## 提示

在显示每次还款后的余额时保留两位小数。提示：每个月的贷款余额减去还款金额后，还需要加上贷款余额与月利率的乘积。月利率的计算方法是把用户输入的利率转换成百分数再除以 12。