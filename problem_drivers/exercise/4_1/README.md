# 4_1 逆序打印 Reverse print

## 题目

编写一个程序，要求用户输入一个两位数，然后按数位的逆序打印出这个数。

## 样例

### 样例一

      Enter a two-digit number: 28
      The reversal is: 82

### 样例二

      Enter a two-digit number: 35
      The reversal is: 53

## 数据范围

输入的是两位数的整数。
按照题目内容中的输出为规范输出。

## 提示

用 %d 读入两位数，然后分解成两个数字。




