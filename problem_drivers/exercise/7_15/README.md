### 7-15：

 

编程计算正整数的阶乘：
	
		Enter a positive integer: 6
		Factorial of 6: 720
(a) 用 short 类型变量存储阶乘的值。为了正确打印出n的阶乘，n的最大值是多少？ 7 
(b) 用 int 类型变量重复(a)。31
(c) 用long 类型变量重复(a)。31
(d) 如果你的编译器支持 long long 类型，用 long long 类型变量重复(a)。31
(e) 用 float 类型变量重复(a)。16
(f) 用 double 类型变量重复(a)。61
(g) 用 long double 类型变量重复(a)。
在 (e)~(g) 几种情况下，程序会显示阶乘的近似值，不一定是准确值。
请把你的答案替换程序中的 a、b、c、e、f 中并且提交
		#include <stdio.h>
		int main(void)
		{
		printf("%d,%d,%d,%d,%d,%d",a,b,c,d,e,f);
		return 0;
		}