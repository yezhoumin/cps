# 项目2.8: 编写程序显示单月的日历。用户指定这个月的天数和该月起始日是星期几.

```
Enter number of days in month：31
Enter starting day of the week(1=Sum, 7=Sat): 3
       1  2  3  4  5
6  7  8  9  10 11 12
13 14 15 16 17 18 19
20 21 22 23 24 25 26
27 28 29 30 31
```
