### 项目4.4：编写程序读一条消息,然后逆序打印出这条消息:

        Enter a message: Don't get mad, get even.
        Reversal is: .neve teg ,dam teg t'noD

提示:一次读取消息中的一个字符(用`getchar`函数),并且把这些字符存储在数组中,你*必须*用指针代替整数来跟踪数组中的当前位置。当数组满了或者读到字符`'\n'`时停止读操作。